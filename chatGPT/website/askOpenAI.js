// Setze deine OpenAI API-Zugriffsschlüssel
const apiKey = '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';
const apiUrl = 'https://api.openai.com/v1/engines/davinci-codex/completions';

// Funktion zum Senden einer Anfrage an die OpenAI API
async function sendOpenAIRequest(prompt) {
  try {
    const response = await axios.post(apiUrl, {
      prompt: prompt,
      max_tokens: 100,
      temperature: 0.6,
      n: 1,
      stop: null
    }, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer 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`
      }
    });

    const { choices } = response.data;
    const { text } = choices[0];
    return text.trim();
  } catch (error) {
    console.error('Fehler beim Senden der OpenAI-Anfrage:', error);
    return null;
  }
}

// Funktion zum Stellen der Frage an OpenAI
async function askQuestion() {
  const questionInput = document.getElementById('question');
  const question = questionInput.value;
  const prompt = `Frage: ${question}\nAntwort:`;

  const openAIResponse = await sendOpenAIRequest(prompt);
  if (openAIResponse) {
    const answerDiv = document.getElementById('answer');
    answerDiv.textContent = openAIResponse;
  } else {
    console.log('Fehler beim Abrufen der Antwort von OpenAI.');
  }
}