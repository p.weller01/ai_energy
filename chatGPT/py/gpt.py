import openai
import os
import time
from getTemp import queryTemp
from urllib import request
import json

OPEN_API_KEY_PAYED = "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"
OPEN_API_KEY_FREE = "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"

temperature = queryTemp() # in °C
raum = 20 #in m^2
energieffizienzKlasse = 'A' 
verbrauch_pro_grad = 6


# Verbindung zu OpenAI herstellen
openai.api_key = OPEN_API_KEY_FREE


def ask(msg):
    response = openai.Completion.create(
        engine="text-davinci-002",
        prompt=msg,
        max_tokens=2024,
        n = 1,
        stop = None,
        temperature=0.7,
    )
    return response.choices[0].text

def getData(url):
    # Einlesen der Werte als String
    rueckgabe = request.urlopen(url)
    inhalt = rueckgabe.read()
    inhalt_text = inhalt.decode("UTF-8")

    # String in JSON umwandeln
    inhalt_json = json.loads(inhalt_text)

    return inhalt_json

# Antwort von ChatGPT ausgeben
run = True
#print(getData("https://www.empro.phoenixcontact.com/api/v1/measurements/f?"))

while run == False:
    print("-----------------------------------------------------------------")
    print(temperature)
    response = ask(f"In meinem Zimmer ist eine Temperatur von {temperature}°C, bei einem Verbrauch von 2kg CO2 pro °C verbrauche ich wie viel CO2?")
    print(response.choices[0].text)
    run=False
    time.sleep(5)

