function update()
{
    heat();
    eff();
    space();
    tempIst();
    tempSoll();
}
function heat()
{
    var e=document.getElementById('heat');
    var selected = e.options[e.selectedIndex].text;
    document.getElementById("varheat").innerHTML = selected;
}
function eff()
{
    var e=document.getElementById('eff');
    var selected = e.options[e.selectedIndex].text;
     document.getElementById("vareff").innerHTML = selected;
}
function space()
{
    var e=document.getElementById('space');
    var value = e.value;
    document.getElementById("varspace").innerHTML = value;
}

function tempIst()
{
    var e=document.getElementById('tempIst');
    var value = e.value;
    document.getElementById("vartempist").innerHTML = value;
}

function tempSoll()
{
    var e=document.getElementById('tempSoll');
    var value = e.value;
    document.getElementById("vartempsoll").innerHTML = value;
}