import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
import numpy as np
bucket = "AIEnergy"
org = "bbs2wob"
token = "WC9pXuxWVZidd0NRWS1WekG3_GaDAjV7vsqxCxj6eSBisJaCZ7rCQDsU5kvEgNM0KMIKXm2RJrpLLYpgGMm00A=="
# Store the URL of your InfluxDB instance
url="https://influx.xplore-dna.de"

client = influxdb_client.InfluxDBClient(
    url=url,
    token=token,
    org=org
)


def queryTemp():
    # Query script
    query_api = client.query_api()
    query = 'from(bucket:"AIEnergy")\
            |> range(start: -10m)\
            |> filter(fn:(r) => r.domain == "climate")\
            |> filter(fn:(r) => r._field == "current_temperature")'
    result = query_api.query(org=org, query=query)
    results = []
    for table in result:
        for record in table.records:
            results.append(record.get_value())
    result = 0
    
    return np.around(np.mean(results),2)