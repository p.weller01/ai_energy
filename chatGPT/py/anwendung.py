from tkinter import * 
from gpt import ask
from getTemp import queryTemp

# Ein Fenster erstellen
fenster = Tk()

fenster.geometry("900x400")
# Den Fenstertitle erstellen
fenster.title("AI-Energy: Heizungsoptimierung")

# Heizungsart
label_heiz = Label(fenster,text="Heizungsart") 
clicked_heiz = StringVar()
heizungstpyen = [
    "Fernwärme",
    "Öl",
    "Gas",
    "Elektrisch",
    "Wärmepumpe"
]
clicked_heiz.set(heizungstpyen[0])
drop_heizung = OptionMenu(fenster,clicked_heiz, *heizungstpyen)
label_heiz.grid(row=0,column=0,pady=20)
drop_heizung.grid(row=0,column=1)

# Effizienz
label_eff = Label(fenster,text="Effizienzklasse wählen") 
clicked_eff=StringVar()
effizienzklassen = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G"
]
clicked_eff.set(effizienzklassen[0])
drop_effizienz = OptionMenu(fenster,clicked_eff, *effizienzklassen)
label_eff.grid(row=1,column=0,pady=20)
drop_effizienz.grid(row=1,column=1)

# Raumgröße
label_raum = Label(fenster, text="Raum")
eingabe_raum = Entry(fenster, bd=5, width=40)
label_raum.grid(row=2,column=0,pady=20)
eingabe_raum.grid(row=2,column=1)

# temp ist
tempi = queryTemp()
label_tempi1 = Label(fenster, text="Aktuelle Temperatur")
label_tempi2 = Label(fenster, text=tempi)
label_tempi1.grid(row=3,column=0,pady=20)
label_tempi2.grid(row=3,column=1)

# temp soll
label_temps = Label(fenster, text="gewünschte Temperatur")
eingabe_temps = Entry(fenster, bd=5, width=40)
label_temps.grid(row=4,column=0,pady=20)
eingabe_temps.grid(row=4,column=1)



# ausgabe

label_ausgabe = Label(fenster,text="")
label_frage = Label(fenster, text="Frage",width=40)

if False:
    label_frage.grid(row=5,column=0,pady=20)
    label_ausgabe.grid(row=5,column=1)

def changeAusgabe(text):
    label_ausgabe.config(text=text)



# In der Ereignisschleife auf Eingabe des Benutzers warten.
changeAusgabe("Hallo")


antwort = Label(fenster,text=" antwort")

# Button
def button_action():
    ausgabe = ["Ich habe einen Raum der mit einer "+clicked_heiz.get()+"\n"+" Heizung."+
              "Mein Raum ist "+eingabe_raum.get()+" m^2 groß."+"\n"+
              "Meine Heizung hat die Effizienzklasse "+clicked_eff.get()+"\n"+
              "Wenn ich die Temperatur von "+ str(label_tempi2.cget("text"))+"°C auf"+"\n"+ eingabe_temps.get()+
              "°C ändere, spare ich wie viel CO2 spare ich pro Tag ein?"+"\n"+
              "Arbeite mit diesen Informationen,"+"\n" +"mehr Informationen habe ich nicht."+"\n"+
              " Antwort in maximal 20 Zeichen. Schätze das Ergebnis,"+"\n"+
              " wenn dir Datenfehlen. Ich brauche einen Zahlenwert."]
          
    changeAusgabe(ausgabe)
    text= ask(ausgabe)
    antwort.config(text=text)
    

but = Button(fenster, text="Berechnen",command=button_action)
but.grid(row=6)

antwort.grid(row=0,column=2)

    

fenster.mainloop()