# Max Weller, 21.02.2023

import influxdb_client as db
from influxdb_client.client.write_api import SYNCHRONOUS
import time
from datetime import datetime
import json
from urllib import request
from bot import send_message_to_Group
from check import check
from ai import analyse


file_Data = open('C:\\Users\\maxim\\OneDrive\\Arbeit\\AI_Energy\\ai_energy\\raspberry\\writeData.json')
data = json.load(file_Data)



# relevate Infos zum verbinden mit der DB
bucket = data["bucket"]
org = data["org"]
url = data["url"]
token = data["token"]

# Links zu den Daten
links = []
for x in data["measurements"]:
    links.append(data["IP_test"]+x)

# Zeitintervall einlesen
timeIntervall = data["timeIntervall"]

client = db.InfluxDBClient(
    url = url,
    token = token,
    org = org
)

run = True
print("Gestartet")

def getData(url):
    # Einlesen der Werte als String
    rueckgabe = request.urlopen(url)
    inhalt = rueckgabe.read()
    inhalt_text = inhalt.decode("UTF-8")

    # String in JSON umwandeln
    inhalt_json = json.loads(inhalt_text)

    return inhalt_json

def writeData(url):    
    # Werte in DB schreiben
    inhalt_json = getData(url)
    write_api = client.write_api(write_options=SYNCHRONOUS)
    p = db.Point("test_F").measurement("measurement").field(inhalt_json["name"],inhalt_json["value"])
    write_api.write(bucket=bucket,org=org,record=p)
    fine = check(inhalt_json["name"],inhalt_json["value"])
    msg = ""
    if fine == False:
        msg = inhalt_json["name"]+": "+str(inhalt_json["value"])+inhalt_json["unit"]
    return msg

def main():
    try: #testet ob Internet vorhanden ist
        nowtime = datetime.now().strftime("%d/%m/%Y %H:%M:%S") # stellt aktuelle Zeit dar
        for x in links:
            #print(x)
            writeData(x)
        print("in DB geschrieben")
        err = analyse(test=True) # AI Fehleranalyse
        if(len(err) != 0): # Fehler gefungen
            fine = check("P",max(err)) # gucken ob der Fehler ok ist
        if(fine): # Fehler ist ok
            print("i.O.") 
        else: # Fehler ist nicht ok
            send_message_to_Group(nowtime+" Fehlerhafter Messwert: P=" +str(max(err)))
            print(nowtime+" Fehlerhafter Messwert: P=" +str(max(err)))
    except:
        print("Ferhler")

while run == True:
    main()
    time.sleep(timeIntervall)
    
