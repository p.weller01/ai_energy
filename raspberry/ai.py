import numpy as np
from sklearn.ensemble import IsolationForest
from getData import getData


def analyse(test):
    # Beispiel-Messwerte
    if(test):
        data = [1.2, 1.5, 1.3, 2.1, 1.8, 50000000, 1.4, 1.6, 1.7, 1.9]
    else:
        data = getData(bucket='phoenix', measurement='measurement', field='P', time='1m')
    #print(data)

    # Konvertiere die Daten in ein Numpy-Array
    data_array = np.array(data).reshape(-1, 1)
    # Initialisiere den IsolationForest
    clf = IsolationForest()

    # Trainiere den IsolationForest mit den Daten
    clf.fit(data_array)

    # Verwende den trainierten IsolationForest, um Ausreißer zu erkennen
    outliers = clf.predict(data_array)

    # Markiere die Ausreißer in den Daten
    marked_data = []
    err_data = []
    for i, val in enumerate(data):
        if outliers[i] == -1:
            marked_data.append("*" + str(val))  # Markiere Ausreißer mit einem Sternchen
            err_data.append(val)
        else:
            marked_data.append(str(val))

    # Gib die markierten Daten aus
    if(test):
        print("Markierte Daten:")
        print(marked_data)

    return err_data
